package uk.co.miki.ebiz.di.booking.email;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EmaiCreator 
{
    private static ApplicationContext context;
	
	public static void sendEmail(String to[], String subject, String msg){
    	context = new ClassPathXmlApplicationContext("spring-mail.xml");
    	 
    	EmailSender mm = (EmailSender) context.getBean("mailMail");
    	mm.sendMail(to, subject,msg);
        
    }
}