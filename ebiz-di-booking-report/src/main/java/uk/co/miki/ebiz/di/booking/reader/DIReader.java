package uk.co.miki.ebiz.di.booking.reader;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import uk.co.miki.ebiz.di.booking.dao.DIBookingDAO;
import uk.co.miki.ebiz.di.booking.domain.DIBooking;
import uk.co.miki.ebiz.di.booking.exception.FatalException;

public class DIReader extends AbstractItemCountingItemStreamItemReader<Map<String, List<DIBooking>>>{
	private static final Logger logger = LoggerFactory.getLogger(DIReader.class);

	@Value("${di.booking.report.days}")
	private int reportDays;
	
	@Autowired
	private DIBookingDAO diBookingDAO;
	
	@Override
	public Map<String, List<DIBooking>> doRead() throws FatalException{
		logger.info("Start DI Booking Report Reader");
		int numberOfDays = reportDays;
		if(LocalDate.now().getDayOfWeek().equals(DayOfWeek.MONDAY)){
			numberOfDays  = numberOfDays + 2;
		}
		Map<String, List<DIBooking>> diBookings  = new HashMap<String, List<DIBooking>>();
		if(getCurrentItemCount() > 1 ) {
			return null;
		}
		diBookings.put("test", diBookingDAO.getDIBookings("test", numberOfDays));
		diBookings.put("staging", diBookingDAO.getDIBookings("staging", numberOfDays));
		return diBookings;
	}
	
	@Override
	protected void doClose() throws Exception {
	}

	@Override
	protected void doOpen() throws Exception {
	}	

}
