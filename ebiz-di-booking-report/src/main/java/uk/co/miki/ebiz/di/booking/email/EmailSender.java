package uk.co.miki.ebiz.di.booking.email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EmailSender
{
	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;
	
	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendMail(String to[], String subject, String msg) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		try{
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			message.setTo(to);
			message.setSubject(subject);
			message.setText(msg, true);
			mailSender.send(mimeMessage);	
		}catch(MessagingException  ee){
			ee.printStackTrace();
		}
	}
	
	
}
