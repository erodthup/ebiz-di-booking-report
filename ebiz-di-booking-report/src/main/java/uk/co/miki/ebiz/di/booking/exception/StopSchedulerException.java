package uk.co.miki.ebiz.di.booking.exception;

public class StopSchedulerException  extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public StopSchedulerException(String msg) {
		super(msg);
	}
	
	public StopSchedulerException(Throwable cause) {
		super(cause);
	}

	public StopSchedulerException(String msg, Throwable cause) {
		super(msg, cause);
	}	
}
