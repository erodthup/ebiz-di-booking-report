package uk.co.miki.ebiz.di.booking.writer;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;

import uk.co.miki.ebiz.di.booking.domain.DIBooking;
import uk.co.miki.ebiz.di.booking.email.EmaiCreator;

public class DIWriter implements ItemWriter<Map<String, List<DIBooking>>>{
	Logger logger = LoggerFactory.getLogger(DIWriter.class);
	
	@Value("${supplier.no.uat}")
	private String suppliersNoUAT;
	
	public void write(List<? extends Map<String, List<DIBooking>>> batchInput){
		logger.info("Process ARIWriter Start");
		Map<String, List<DIBooking>> bookingMaps = batchInput.get(0);
		String emailto = "";
        try {
			emailto = FileUtils.readFileToString(new File("emailTo.txt").getAbsoluteFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
        if(StringUtils.isNotBlank(emailto)){
			String[] emails = emailto.split(",");
			String msg = "<font style='font-size:16px; color:#151e1e;' ><b>Daily report confirmed/on-error DI bookings in test/staging</b></font><br/>";
			msg += "<font style='font-size:14px; color:#151e1e;' > <b> Date: " + LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + "</b></font><br/><br/>";
			
			for (Map.Entry<String, List<DIBooking>> entry : bookingMaps.entrySet()) {
				List<DIBooking> bookings = entry.getValue();
				String envName = "<font style='font-size:14px'> <b> " + entry.getKey().toUpperCase() + "</b></font><br/>";
				if(bookings.size() > 0){
					
					String table = "<table  border='0' style='border-spacing: 1px; color:white; font-size:12px' width='100%'><tr bgcolor='#476b6b'><th>Booking Ref</th><th>Tour Number</th><th>Booking Status</th><th>Supplier Id</th><th>Client Ref</th><th>Booking Date</th></tr>%data%</table>";
					String row = "<tr bgcolor='#85adad' align='center'><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";
					String rowNoUAT = "<tr bgcolor='#cc2a2a' align='center'><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";
					Set<String> supplierNoUAT = new HashSet<String>(Arrays.asList(suppliersNoUAT.split(",")));
					StringBuffer allBookings = new StringBuffer("");
					for(DIBooking diBooking: bookings){
						String diBookingRow = supplierNoUAT.contains(diBooking.getSupplierId()) ? rowNoUAT : row;
						String booking = String.format(diBookingRow, diBooking.getBookingref(), diBooking.getTournumber(),
								diBooking.getBookingstatus(), diBooking.getSupplierName(), diBooking.getClientref(),
								diBooking.getBookingdate().toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
						allBookings.append(booking);
					}
					table = table.replaceAll("%data%", allBookings.toString());
					msg = msg + envName + table + "<br/><br/>";
				}else{
					msg = msg + envName + "No Result" + "<br/><br/>";
				}
				
			}
		
		EmaiCreator.sendEmail(emails, "Daily report confirmed/on-error DI bookings in test/staging (" + LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE) + ")", msg);
        }
		logger.info(" DI Booking End");
	}
	
	
	

}
