package uk.co.miki.ebiz.di.booking.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

import uk.co.miki.ebiz.di.booking.domain.DIBooking;


@Component
@Qualifier("diBookingDAO")
public class DIBookingDAO extends JdbcDaoSupport {

	Logger logger = LoggerFactory.getLogger(DIBookingDAO.class);		

	@Autowired
	@Qualifier("testDataSource")
	private DataSource testDataSource;
	
	@Autowired
	@Qualifier("stagingDataSource")
	private DataSource stagingDataSource;

	private String SELECT = " SELECT distinct h.bookingref, d.tournumber, d.bookingstatus, s.supplierid, sic.SUPPLIERINTERFACENAME, h.clientref, d.timeadded "
			+ "FROM bookingdetail d JOIN bookingheader h ON h.bookingid = d.bookingheaderid "
			+ "JOIN bookingdetailsupplier s ON s.detailid = d.detailid AND s.supplierid NOT IN ('01', '43') "
			+ "JOIN supplierinterfacecontrol sic on sic.SUPPLIERINTERFACECODE = s.supplierid "
			+ "WHERE d.bookingstatus IN ('99', '30', '10') AND DATE(d.timeadded) >= (CURRENT DATE - ? DAYS) AND d.latest = '1' order by d.timeadded desc";

	@PostConstruct
	protected void init() {
		setDataSource(testDataSource);
	}

	
	public List<DIBooking> getDIBookings(String env, int numberOfDays) {
		if("staging".equalsIgnoreCase(env)){
			setDataSource(stagingDataSource);
		}else{
			setDataSource(testDataSource);
		}
		try{
			return getJdbcTemplate().query(
					SELECT, new Object[]{numberOfDays},  (rs, index) ->  
					new DIBooking(
							rs.getString("bookingref").trim(), 
							rs.getString("tournumber"), 
							rs.getString("bookingstatus"), 
							rs.getString("SUPPLIERINTERFACENAME") + " [" + rs.getString("supplierid")+"]", 
							rs.getString("supplierid"), 
							rs.getString("clientref"), 
							rs.getTimestamp("timeadded")
							));
	
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}

}
