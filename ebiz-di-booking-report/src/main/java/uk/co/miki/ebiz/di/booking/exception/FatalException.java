package uk.co.miki.ebiz.di.booking.exception;

import org.quartz.JobExecutionException;

public class FatalException extends JobExecutionException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CacheNotFoundException.
	 * @param msg the detail message
	 */
	public FatalException(String msg) {
		super(msg);
	}
	
	public FatalException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor for CacheNotFoundExceptiona.
	 * @param msg the detail message
	 * @param cause the root cause 
	 * 
	 */
	public FatalException(String msg, Throwable cause) {
		super(msg, cause);
	}	

}
