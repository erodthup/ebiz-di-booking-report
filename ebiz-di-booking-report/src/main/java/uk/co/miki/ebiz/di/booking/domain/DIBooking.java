package uk.co.miki.ebiz.di.booking.domain;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor 
public @Data class DIBooking {
	private String bookingref;
	private String tournumber;
	private String bookingstatus;
	private String supplierName;
	private String supplierId;
	private String clientref;
	private Timestamp bookingdate;
	
}
